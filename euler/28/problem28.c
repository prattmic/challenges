/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 28
 * Sum the corners of a 1001 by 1001 spiral */

#include <stdlib.h>
#include <stdio.h>

unsigned long spiral = 1;
unsigned long sum = 1;

int main(void) {
    int side = 3;
    int i = 0;

    while (side < 1002) {
        spiral += side - 1;
        sum += spiral;
        i += 1;
        if (i == 4) {
            side += 2;
            i = 0;
        }
    }

    printf("%ld\n", sum);

    return EXIT_SUCCESS;
}
