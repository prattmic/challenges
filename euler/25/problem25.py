#!/usr/bin/env python
# Michael Pratt <michael@pratt.im>
# Project Euler - Problem 25
# First fibonacci number with 1000 digits

a = 0
b = 1
count = 0

while len(str(a)) < 1000:
    temp = b
    b = b + a
    a = temp
    count += 1

print(count)
