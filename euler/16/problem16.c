/* Michael Pratt <michael@prattmic.com>
 * Project Euler - Problem 16
 * Sum the digits of 2^1000 */

#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <sys/resource.h>
#include <errno.h>


int main(void) {
    int cmp;
    mpz_t answer;
    mpz_t power;
    mpz_init(answer);
    mpz_init(power);

    struct rlimit limit;
        
    /* Set the file size resource limit. */
    limit.rlim_cur = 64L * 1024L * 1024L;
    limit.rlim_max = 64L * 1024L * 1024L;
    if (setrlimit(RLIMIT_FSIZE, &limit) != 0) {
        printf("setrlimit() failed with errno=%d\n", errno);
    }

    /* Get the file size resource limit. */
    if (getrlimit(RLIMIT_FSIZE, &limit) != 0) {
        printf("getrlimit() failed with errno=%d\n", errno);
    }

    printf("The soft limit is %llu\n", limit.rlim_cur);
    printf("The hard limit is %llu\n", limit.rlim_max);

    /*const rlim_t kStackSize = 64L * 1024L * 1024L;   * min stack size = 64 Mb *
    struct rlimit rl;
    int result;

    rl.rlim_cur = kStackSize;
    result = setrlimit(RLIMIT_STACK, &rl);
    if (result != 0)  {
        fprintf(stderr, "setrlimit returned result = %d\n", result);
        perror("error");
    } */
    
     

    mpz_ui_pow_ui(power, 2, 1000);

/*    printf("here"); */
    gmp_printf("%Zd\n", power);

    cmp = mpz_cmp_ui(power, 1);
    if (cmp > 0) {
        mpz_t mod;
        mpz_mod_ui(mod, power, 10);
    }
    /*
    while (1) {  * power > 1 *
        mpz_t mod;
        printf("here");
        mpz_mod_ui(mod, power, 10);     * power % 10 *
        mpz_add(answer,answer,mod);     * result += mod *
        gmp_printf("%Zd <-- %Zd\n", mod, power);
        mpz_tdiv_q_ui(power, power, 10);     * power/=10 *
    }

    gmp_printf("%Zd\n", answer); */

    printf("here\n");

    return EXIT_SUCCESS;
}

