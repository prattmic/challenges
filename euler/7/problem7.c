/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 7
 * Find the 10001st prime */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int testprime(long num);

int main(void) {
    long num = 1;
    int count = 0;

    for (count = 0; count < 10001; num += 2) {
        if (testprime(num)) {
            count++;
        }
    }

    printf("%ld\n", num-2);

    return EXIT_SUCCESS;
}

int testprime(long num) {
    int i;
    for (i = sqrt(num); i > 1; i--) {
        if (num % i == 0) {
            return 0;
        }
    }
    return 1;
}
