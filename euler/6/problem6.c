/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 6
 * Difference between the sum of squares and square of sums */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int squaresum = 0;
    int normsum = 0;
    int i;

    for (i = 1; i <= 100; i++) {
        squaresum += i*i;
        normsum += i;
    }

    printf("%d\n", normsum*normsum - squaresum);

    return EXIT_SUCCESS;
}
