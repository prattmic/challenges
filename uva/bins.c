/* Michael Pratt <michael@pratt.im>
 * Bin Sorting Problem
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=3&page=show_problem&problem=38 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/* bin[0][B,G,C], bin[1][B,G,C], bin[2][B,G,C] */
int bin[3][3];

int moves(int, int, int);

int main(void) {

    /* Parse each line */
    while (scanf("%d %d %d %d %d %d %d %d %d\n", &bin[0][0], &bin[0][2], &bin[0][1], &bin[1][0], &bin[1][2], &bin[1][1], &bin[2][0], &bin[2][2], &bin[2][1]) == 9) {    /* Switches C and G */
        int smallest[4] = {-1, -1, -1, INT_MAX};
        int i, j, k;

        for (i = 0; i <= 2; i++){
            for (j = 0; j <= 2; j++) {
                if (j != i) {               /* Can't use same color twice */
                    for (k = 0; k <= 2; k++) {
                        if (k != j && k != i) {     /* Can't use same color twice */
                            int result = moves(i,j,k);
                            if (result < smallest[3]) {     /* Smallest! */
                                smallest[0] = i;
                                smallest[1] = j;
                                smallest[2] = k;
                                smallest[3] = result;
                            }
                        }
                    }
                }
            }
        }

        for (i = 0; i <= 2; i++) {
            switch (smallest[i]) {
                case 0:
                    printf("B");
                    break;
                case 1:
                    printf("C");
                    break;
                case 2:
                    printf("G");
                    break;
            }
        }
        printf(" %d\n", smallest[3]);
    }

    return EXIT_SUCCESS;
}

/* Calculates the moves required to sort.
 * Arguments are the box order, where
 * B = 0, C = 1, G = 2 */
int moves(int x, int y, int z) {
    int count = 0;
    int i;

    /* First box */
    for (i = 0; i <= 2; i++) {
        if (i != x) {
            count += bin[0][i];
        }
    }

    /* Second box */
    for (i = 0; i <= 2; i++) {
        if (i != y) {
            count += bin[1][i];
        }
    }

    /* Third box */
    for (i = 0; i <= 2; i++) {
        if (i != z) {
            count += bin[2][i];
        }
    }

    return count;
}
