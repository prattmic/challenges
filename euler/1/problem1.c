/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 1
 * Find the sum of all multiples of 3 and 5 below 1000 */

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

int sumtwopass(void);
int sumonepass(void);

int main(void) {

    int msec;
    clock_t start = clock(), diff;
    int i = 0;
    for (i = 0; i < 1000000; i++) {
        sumtwopass();
    }
    diff = clock() - start;

    msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("Two Pass: %d\n", sumtwopass());
    printf("Time taken %d seconds %d milliseconds\n\n", msec/1000, msec%1000);


    
    start = clock();
    for (i = 0; i < 1000000; i++) {
        sumonepass();
    }
    diff = clock() - start;

    msec = diff * 1000 / CLOCKS_PER_SEC;
    printf("One Pass: %d\n", sumonepass());
    printf("Time taken %d seconds %d milliseconds\n", msec/1000, msec%1000);



    return EXIT_SUCCESS;
}

int sumtwopass(void) {
    int i = 0;
    int sum = 0;

    /* Multiples of 3 */
    for (i = 0; i<1000; i+=3) {
        sum += i;
    }

    /* Multiples of 5 */
    for (i = 0; i<1000; i+=5) {
        if (i % 3) {               /* Don't double count multiples of 3 and 5 */
            sum += i;
        }
    }

    return sum;
}

int sumonepass(void) {
    int i = 0;
    int sum = 0;

    for (i = 0; i<1000; i++) {
        if (i % 3 == 0) {
            sum = sum + i;
            continue;
        }
        else if (i % 5 == 0) {
            sum = sum + i;
        }
    }

    return sum;
}
