/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 14
 * Find the longest chain to get to one */

#include <stdlib.h>
#include <stdio.h>

#define  MILLION    1000000

int main(void) {
    int start = MILLION - 1;
    long maxlength = 0;
    int maxstart;

    while (start > 0) {
        long chain = start;
        long length = 0;
        while (chain != 1) {
            if (chain % 2) {        /* odd */
                chain = 3*chain + 1;
            }
            else {                  /* even */
                chain /= 2;
            }
            length++;
        }
        if (length > maxlength) {
            maxstart = start;
            maxlength = length;
        }
        start -= 2;
    }

    printf("%d\n", maxstart);

    return EXIT_SUCCESS;
}
