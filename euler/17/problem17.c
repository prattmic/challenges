/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 17
 * Count the letters required to write out all numbers 1 to 1000 */

/* Answer is off by +10, I must be double counting somewhere */

#include <stdlib.h>
#include <stdio.h>

int letters[][10] = { {0 /* zero */, 3 /* one */, 3 /* two */, 5 /* three */, 4 /* four */, 4 /* five */, 3 /* six */ , 5 /* seven */, 5 /* eight */, 4 /* nine */},
                      {0, 3 /* teens (add +1 for 14,16,17,18,19) */, 6 /* twenty */, 6/* thirty */, 5 /* forty */, 5 /* fifty */, 5 /* sixty */, 7 /* seventy */, 6 /* eighty */, 6 /* ninety */} };

int main(void) {
    int i = 1;
    int count = 0;

    for (i = 1; i <= 1000; i++) {
        int j = i;
        int oldcount = count;
        count += letters[0][i%10];      /* Lowest digit */

        if (i >= 10) {                  /* Teens */
            int mod;
            j /= 10;
            mod = j%10;
            count += letters[1][mod];      
            if (i%100 == 14 || i%100 == 16 || i%100 == 17 || i%100 == 18 || i%100 == 19) {
                count += 1;
            }
        }

        if (i >= 100) {                 /* Hundreds */
            if (i % 100) {              /* If number is not round hundred, add "and" */
                count += 3;
            }
            count += 7;                 /* "hundred" */
            j = i / 100;                /* just get top digit */
            count += letters[0][j];     /* treat as single number */
        }

        if (i >= 1000) {
            count += 4;                /* Lazy.... */
        }

        printf("%d has %d letters\n", i, count-oldcount);
    }

    printf("%d\n", count);

    return EXIT_SUCCESS;
}
