/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 3 
 * Find the largest prime factor of a composite number */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

long input = 600851475143;

long largest_prime_fac(long num);
int test_prime(long num);

int main(void) {
    printf("%ld\n", largest_prime_fac(input));

    return EXIT_SUCCESS;
}

long largest_prime_fac(long num) {
    long factors[100] = { 1, 0 };
    long *fac;
    long i;
    long big_prime = 0;

    fac = &factors[1];

    for (i = 1; i <= sqrt(num); i++) {
        if (num % i == 0) {
            *fac = i;
            fac++;
        }
    }

    fac = factors;
    while (*(++fac)) {
        if (test_prime(*fac)) {
            big_prime = *fac;
        }
    }

    return big_prime;
}

int test_prime(long num) {
    long i;

    for (i = 2; i <= sqrt(num); i++) {
        if (num % i == 0) {
            return 0;                       /* Not prime */
        }
    }

    return 1;                               /* If we made it through, it is prime */
}
