/* Michael Pratt <michael@pratt.im>
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=29&page=show_problem&problem=1078
 * The Trip - Total amount that must be exchanged to equalize costs */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(void) {
    int n;

    while (scanf("%d\n", &n) == 1) {
        float *list, *begin;
        float sum, total = 0;
        float average;
        int i;

        if (n == 0) {   /* End of list */
            break;
        }

        list = calloc(sizeof(float*), n);
        begin = list;

        for (i = 0; i < n; i++) {
            float num;
            if (scanf("%f\n", &num) != 1) {
                fprintf(stderr, "Input error on cost list.");
                exit(1);
            }
            *(list++) = num;
        }

        list = begin;
        sum = 0;
        for (i = 0; i < n; i++) {
            sum += *(list++);
        }
        average = sum / n;
        average = ceil(100 * average) / 100;   /* Round to 2 decimal places */

        list = begin;
        for (i = 0; i < n; i++) {
            if (list[i] > average) {
                total += list[i] - average;
            }
        }
        printf("$%.2f\n", total);
    }

    return EXIT_SUCCESS;
}
