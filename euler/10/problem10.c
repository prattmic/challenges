/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 10 
 * Find the sum of all primes below two million */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int testprime(long num);

int main(void) {
    long sum = 2;
    long candidate = 3;

    for (candidate = 3; candidate < 2000000; candidate += 2) {
        if (testprime(candidate)) {
            sum += candidate;
        }
    }

    printf("%ld\n", sum);

    return EXIT_SUCCESS;
}

int testprime(long num) {
    int i;
    for (i = sqrt(num); i > 1; i--) {
        if (num % i == 0) {
            return 0;
        }
    }
    return 1;
}
