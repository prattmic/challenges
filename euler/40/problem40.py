#!/usr/bin/env python
# Michael Pratt <michael@pratt.im>
# Project Euler - Problem 40
# Multiply parts of irrational fraction

import random

fraction = [random.randint(0,9) for i in range(0,1000001)]

multi = fraction[1] * fraction[10] * fraction[100] * fraction[1000] * fraction[100000] * fraction[1000000]

print(multi)
