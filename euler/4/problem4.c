/* Michael <michael@pratt.im>
 * Project Euler - Problem 4
 * Find the largest palindrome made from the product of two 3-digit numbers */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int x, y;
    int bigpal = 0;

    for (x = 100; x < 1000; x++) {
        for (y = 100; y < 1000; y++) {
            int multi;
            int rtemp;
            int reverse = 0;
            multi = x*y;

            rtemp = multi % 1000;       /* Lowest three digits */
            while (rtemp) {
                reverse *= 10;
                reverse += rtemp % 10;
                rtemp /= 10;
            }

            if ( ( (multi/1000) == (reverse) ) && (multi > bigpal) ) {
                bigpal = multi;
            }
        }
    }

    printf("%d\n", bigpal);

    return EXIT_SUCCESS;
}
