/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 9
 * Find a Pythagorean triplet where a + b + c = 1000 */

/* We can substitute in for c, so a + b + sqrt(a^2 + b^2) = 1000 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void) {
    float a = 0.0;
    float b = 1000.0;

    for (b = 1000.0; b > 0.0; b--) {
        for (a = 0.0; a <= 1000.0; a++) {
            float expr = a + b + sqrt(a*a + b*b);

            if (expr == 1000.0) {     /* Done! */
                printf("a = %f, b = %f, c = %f, abc = %f\n", a, b, sqrt(a*a + b*b), a*b*sqrt(a*a + b*b));
            }
            else if (expr > 1000.0) {
                break;              /* Our b is too big, lets go down */
            }
        }
    }

    return EXIT_SUCCESS;
}
