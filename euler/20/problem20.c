/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 20
 * Sum the digits of 100! */

#include <stdio.h>
#include <stdlib.h>

double factorial(int);

int main(void) {
    char string[1000];
    char *ptr;
    int sum = 0;

    sprintf(string, "%.f", factorial(10));

    printf("%s\n", string);

    ptr = &string[0];

    sum = *ptr - '0';
    printf("%d", sum);

    while (*(++ptr) != '\0') {
        printf(" + %d", *ptr - '0');
        sum += *ptr - '0';
    }
    printf("\n%d\n", sum);

    return EXIT_SUCCESS;
}

double factorial(int num) {
    double result = 1;

    while (num > 1) {
        result *= num;
        num--;
    }

    return result;
}
