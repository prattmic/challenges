#!/bin/bash
# Builds the given file.

#colorgcc -Wall -o ./bin/$1 $1.c


colorgcc -lm -ggdb3 -m64 -ansi -pedantic -Wall -Wshadow -Wpointer-arith -Wcast-qual -Wstrict-prototypes -Wmissing-prototypes -o ./bin/${1%.c} ${1%.c}.c

