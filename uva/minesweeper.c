/* Michael Pratt <michael@pratt.im>
 * Minesweeper Generator
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=29&page=show_problem&problem=1130 */

#include <stdlib.h>
#include <stdio.h>

int **getmines(int*, int*);
int **creatematrix(int, int);
void formatfield(int**, int, int);

int main(void) {
    int count = 1;
    int x, y;
    int **mines;

    while ((mines = getmines(&y, &x)) != NULL) {
        if (count != 1) {
            printf("\n");
        }
        printf("Field #%d:\n", count++);
        formatfield(mines, y, x);
    }

    return EXIT_SUCCESS;
}

int **getmines(int *y, int *x) {      
/* Gets next minefield, returns EOF if there are no more */
    char c;
    int i, j;
    int **mines;

    if ((scanf("%d %d\n", y, x) != 2) || (*x == 0 && *y == 0)) {   /* Get matrix size */
        /* fprintf(stderr, "Input error (minefield size).\n"); */
        return NULL;
    }
    mines = creatematrix(*y, *x);

    mines[0][0] = 0;

    /* printf("x = %d, y = %d\n", *x, *y); */

    i = j = 0;

    while ((c = getchar()) != EOF) {
        /* printf("i = %d, j = %d, c = %c\n", i, j, c); */
        switch (c) {
            case '.': {
                mines[i][j] = 0;
                j++;
                break;
            }
            case '*': {
                mines[i][j] = 1;
                j++;
                break;
            }
            case '\n': {
                i++;
                j = 0;
                break;
            }
            default: {      /* Encountered some other character, run!! */
                ungetc(c, stdin);
                goto done;     /* Oh god, please don't kill me,
                                  all I ever wanted was to break a nested loop! */
            }
        }
    }

done:
    return mines;
}

int **creatematrix(int y, int x) {
    int i;
    int **matrix;

    matrix = calloc(sizeof(int*), y);
    for (i = 0; i < y; i++) {
        matrix[i] = calloc(sizeof(int), x);
    }

    return matrix;
}

void formatfield(int **mines, int y, int x) {
    int i, j;
    for (i = 0; i < y; i++) {
        for (j = 0; j < x; j++) {
            if (mines[i][j]) {
                printf("*");
            }
            else {
                int neighbors = 0;
                if (i > 0) {
                    neighbors += mines[i-1][j];
                    if (j > 0) {
                        neighbors += mines[i-1][j-1];
                    }
                    if (j != (x-1)) {
                        neighbors += mines[i-1][j+1];
                    }
                }
                if (i != (y-1)) {
                    neighbors += mines[i+1][j];
                    if (j > 0) {
                        neighbors += mines[i+1][j-1];
                    }
                    if (j != (x-1)) {
                        neighbors += mines[i+1][j+1];
                    }
                }
                if (j > 0) {
                    neighbors += mines[i][j-1];
                }
                if (j != (x-1)) {
                    neighbors += mines[i][j+1];
                }
                printf("%d", neighbors);
            }
        }
        printf("\n");
    }
}
