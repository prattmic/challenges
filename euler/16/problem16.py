#!/usr/bin/env python

# Michael Pratt <michael@pratt.im>
# Project Euler - Problem 16
# Add the digits of 2^1000

two = 2**1000
ten = 10

answer = 0

while two > 1:
    divm = divmod(two,ten)
    print("%d --> %d" % divm)
    answer = answer + divm[1]
    two = divm[0]

print(answer)
