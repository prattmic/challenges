#! /usr/bin/env python

def factorial(num):
    result = 1

    while (num > 1):
        result *= num
        num -= 1

    return result

if __name__ == "__main__":
    fact = factorial(100)
    total = 0

    for i in str(fact):
        total += int(i)

    print(total)
