/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 5
 * Find the smallest number divisible by all ints from 1 to 20 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int i;
    int num = 20;

    while (num) {
        for (i = 1; i <= 20; i++) {
            if (num % i) {
                break;
            }
            else if (i == 20) {
                printf("%d\n", num);
                return EXIT_SUCCESS;
            }
        }
        num += 20;
    }

    return EXIT_SUCCESS;
}   
