/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 2
 * Add up the even terms below 4 mil in the Fib sequence */

#include <stdio.h>
#include <stdlib.h>

int sum_even_fib(void);

int main(void) {
    printf("%d\n", sum_even_fib());

    return EXIT_SUCCESS;
}

int sum_even_fib(void) {
    int sum = 0;
    int new = 1;
    int old = 0;
    int temp = 0;

    while (new < 4000000) {
        temp = new;
        new = new + old;
        old = temp;
        if (new % 2 == 0) {
            sum += new;
        }
    }

    return sum;
}
