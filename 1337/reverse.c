/* Michael Pratt <michael@pratt.im>
 * Reverse a string in place */

#include <stdlib.h>
#include <stdio.h>

void reverse(char *);

int main(void) {
    char str[] = "Hello World!";

    reverse(str);

    printf("%s\n", str);

    return EXIT_SUCCESS;
}

void reverse(char *str) {
    int len = 0;
    char *begin = str;
    int i, j;

    while (*str) {      /* Gets str length */
        len += 1;
        str++;
    }
    str = begin;

    for (i = 0, j = len-1; i < j; i++, j--) {
        char temp = str[i];
        str[i] = str[j];
        str[j] = temp;
    }
}
