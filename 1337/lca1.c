/* Michael Pratt <michael@pratt.im>
 * Lowest Common Ancestor of Binary Search Tree
 * Lowest Common Ancestor of Binary Tree - Part 1
 * http://www.leetcode.com/2011/07/lowest-common-ancestor-of-a-binary-search-tree.html
 * http://www.leetcode.com/2011/07/lowest-common-ancestor-of-a-binary-tree-part-i.html
 */

#include <stdlib.h>
#include <stdio.h>

int list[] = {4,7,8,3,2,12,9,34,67,21,83,41};

void quicksort(int *, int, int);
int partition(int *, int, int);
void swap(int *, int *);
int binsearch(int *,int, int, int);
int lca(int *, int, int, int, int);

int main(void) {
    int i;
    int len = sizeof(list)/sizeof(list[0]);

    quicksort(list, 0, len-1);

    printf("List:\n");
    for (i = 0; i < len; i++) {
        printf("%d\n", list[i]);
    }

    printf("7 is at index %d\n", binsearch(list, 0, len-1, 7));
    printf("67 is at index %d\n", binsearch(list, 0, len-1, 67));
    printf("1 is at index %d\n", binsearch(list, 0, len-1, 1));
    printf("\n");
    printf("LCA of 2 and 7 is %d\n", lca(list, 0, len-1, 2, 7));
    printf("LCA of 41 and 7 is %d\n", lca(list, 0, len-1, 41, 7));
    print(n"LCA of 41 and 67 is %d\n", lca(list, 0, len-1, 41, 67));

    return EXIT_SUCCESS;
}

int lca(int *tree, int p, int r, int a, int b) {
    int mid = (p+r)/2;

    if (a == b) {       /* They are equal stupid */
        return a;
    }
    else if (tree[mid] > a && tree[mid] > b) {  /* OK, both going down */
        lca(tree, p, mid-1, a, b);
    }
    else if (tree[mid] < a && tree[mid] < b) {  /* OK, both going up */
        lca(tree, mid+1, r, a, b);
    }
    else {  /* Diverging, this is least common ancestor */
        return tree[mid];
    }
}

int binsearch(int *tree, int p, int r, int value) {
    int mid = (p+r)/2;

    if (p < r) {
        if (tree[mid] == value) {       /* Found it! */
            return mid;
        }
        else if (tree[mid] > value) {   /* Search below */
            binsearch(tree, p, mid-1, value);
        }
        else if (tree[mid] < value) {   /* Search above */
            binsearch(tree, mid+1, r, value);
        }
    }
    else {  /* Not found! */
        return -1;
    }
}

void quicksort(int *array, int p, int r) {
    int q;
    if (p < r) {
        q = partition(array, p, r);
        quicksort(array, p, q-1);
        quicksort(array, q+1, r);
    }
}

int partition(int *array, int p, int r) {
    int mid = array[r];
    int i;
    int j = p-1;

    for (i = p; i < r; i++) {
        if (array[i] < mid) {
            j += 1;
            swap(&array[i], &array[j]);
        }
    }
    swap(&array[r], &array[j+1]);

    return j+1;
}

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
