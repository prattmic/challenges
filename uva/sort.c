/* Michael Pratt <michael@pratt.im>
 * Implementing Sorting algorithms, again */

#include <stdio.h>
#include <stdlib.h>

void insertion(int *, int);
void selection(int *, int);
void quicksort(int *, int, int);
int partition(int *, int, int);
void swap(int *, int, int);

int main(void) {
    int one[] = {4,6,9,2,4,9,1};
    int onesize = sizeof(one)/sizeof(*one);
    int two[] = {0,5,2,6,9,4,1};
    int twosize = sizeof(two)/sizeof(*two);
    int three[] = {2,8,4,9,4,0,3};
    int threesize = sizeof(three)/sizeof(*three);

    int i;

    printf("Insertion:\n");
    insertion(one, onesize);
    for (i = 0; i < onesize; i++) {
        printf("%d\n", one[i]);
    }

    printf("\nSelection:\n");
    selection(two, twosize);
    for (i = 0; i < twosize; i++) {
        printf("%d\n", two[i]);
    }

    printf("\nQuick:\n");
    quicksort(three, 0, threesize-1);
    for (i = 0; i < threesize; i++) {
        printf("%d\n", three[i]);
    }

    return EXIT_SUCCESS;
}

void insertion(int *list, int size) {
    int i, j;

    for (i = 1; i < size; i++) {
        int item = list[i];
        j = i-1;
        while (item < list[j] && j >= 0) {
            list[j+1] = list[j];
            j--;
        }
        list[j+1] = item;
    }
}

void selection(int *list, int size) {
    int i, j;
    int smallest;

    for (i = 0; i < size; i++) {
        for (j = i; j < size; j++) {
            if (list[j] <= list[i]) {
                smallest = j;
            }
        }
        swap(list, i, smallest);
    }
}

void swap(int *list, int i, int j) {
    int temp = list[i];
    list[i] = list[j];
    list[j] = temp;
}

void quicksort(int *list, int p, int r) {
    if (p < r) {
        int q;
        q = partition(list, p, r);
        quicksort(list, p, q-1);
        quicksort(list, q+1, r);
    }
}

int partition(int *list, int p, int r) {
    int pivot = r;
    int i, j;
    
    j = p-1;

    for (i = p; i < r; i++) {
        if (list[i] < list[pivot]) {
            swap(list, i, j+1);
            j += 1;
        }
    }
    swap(list, pivot, j+1);

    return j+1;
}
