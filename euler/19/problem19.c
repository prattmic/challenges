/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 19 
 * Sundays that fell on the first of the month 1/1/1901 to 31/12/2000 */

#include <stdlib.h>
#include <stdio.h>

static int daytab[2][12] = {
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};

int main(void) {
    int *p;
    int year = 1;
    int month = 1;
    int day = 6;        /* Jan 6, 1901 was the first Sunday */

    int sundays = 0;    /* Sundays on the first of the month */

    int i;
    for (i = 0; i <= 1; i++) {
        int j = 0;
        p = &daytab[i][1];
        while (j < 12) {
            *(p+1) += *p;
            p++;
            j += 1;
            printf("%d\n", *p);
        }
    }

    for (year = 1; year <= 100; year++) {
        int leap = 0;

        if ((year % 4 == 0) && (year % 400)) {      /* Leap years are divisible by 4 but not 400 */
            leap = 1;
        }

        p = &daytab[leap][1];
        day += 7;
    }

    return EXIT_SUCCESS;
}
