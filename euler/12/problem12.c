/* Michael Pratt <michael@pratt.im>
 * Project Euler - Problem 12
 * What is the first triangle number with more than 500 divisors */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int divisors(long);

int main(void) {
    long sum = 0;
    int n = 1;
    int divis;

    while ((divis = divisors(sum)) <= 500) {
        /*printf("%ld has %d divisors\n", sum, divis);*/
        sum = sum + n;
        n++;
    }

    printf("%ld\n", sum);

    return EXIT_SUCCESS;
}

int divisors(long n) {
    long divisor = sqrt(n);
    int count = 0;

    while (divisor > 0) {
        if (n % divisor == 0) {
            count++;
        }
        divisor--;
    }

    return 2*count;
}
