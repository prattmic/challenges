/* Michael Pratt <michael@pratt.im>
 * http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=29&page=show_problem&problem=36
 * 3n + 1 problem */

#include <stdlib.h>
#include <stdio.h>

void longest(int, int);
int threen(int);

int flipped = 0;

int main(void) {
    int i, j;

    while (scanf("%d %d\n", &i, &j) == 2) {
        if (i > j) {
            int temp;
            temp = i;
            i = j;
            j = temp;
            flipped = 1;
        }
        longest(i, j);
    }

    return EXIT_SUCCESS;
}

void longest(int i, int j) {
    int count = 0;

    if (!flipped) {
        printf("%d %d", i, j);
    }
    else {
        printf("%d %d", j, i);
        flipped = 0;
    }

    for (; i <= j; i++) {
        int result;
        result = threen(i);

        if (result > count) {
            count = result;
        }
    }

    printf(" %d\n", count);
}

int threen(int n) {
    int count = 1;

    while (n != 1) {
        if (n % 2) {        /* odd */
            n = 3*n + 1;
        }
        else {
            n = n/2;
        }
        count += 1;
    }
    return count;
}
