/* Michael Pratt <michael@pratt.im>
 * Palindrome Integers
 * http://www.leetcode.com/2012/01/palindrome-number.html */

#include <stdio.h>
#include <stdlib.h>

int reverse(int);
int palindrome(int);

int main(void) {
    printf("7331337 reversed: %d\n", reverse(7331337));
    printf("1000000 reversed: %d\n", reverse(1000000));
    printf("12345 reversed: %d\n", reverse(12345));
    printf("7331337 %s a palindrome.\n", palindrome(7331337) ? "is" : "isn't");
    printf("12345 %s a palindrome.\n", palindrome(12345) ? "is" : "isn't");
    return EXIT_SUCCESS;
}

int reverse(int num) {
    int rev = 0;

    while (num) {
        rev *= 10;
        rev += num % 10;
        num /= 10;
    }

    return rev;
}

int palindrome(int num) {
    int divisor = 1;
    int l, r;
    while (num/divisor >= 10) {
        divisor *= 10;
    }

    while (num) {
        l = num / divisor;
        r = num % 10;
        if (l == r) {
            num = (num % divisor) / 10;
            divisor /= 100;
        }
        else {
            return 0;       /* Fail */
        }
    }

    return 1;
}
