#!/usr/bin/env python

# Michael Pratt <michael@pratt.im>
# 2011-Dec-21
# http://www.pythonchallenge.com/pc/def/map.html

string = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
string2 = "http://www.pythonchallenge.com/pc/def/map.html"

newstring = ""

for index, letter in enumerate(string2):
    if ord(letter) >= ord('a') and ord(letter) <= ord('z'):
        if letter == 'y':
            newstring += 'a'
        elif letter == 'z':
            newstring += 'b'
        else:
            newstring += chr(ord(letter) + 2)
    else:
        newstring += letter

print(newstring)
